# UI application basic design

## 1. Architecture should be the simplest if possible
I adopt ```MVCC(Model-View-Controller-Coordinator)``` plus other modules.

### Directories
Recommended directories for iOS/Android to adopt MVCC:
~~~~~
- ${sourceDirectory}/
  - model/
  - view/
  - controller/
  - coordinator/
  - module/
~~~~~

## 2. Model

### Role     
Model stores properties.

### Example
* I don't use getter/setter methods
```java
// Do not
public class User {
    private String name;

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
}
// Do
public class User {
    public String name;
}
```

* I don't manipulate properties  
Assume that there are some logic even if it's so small, it may be difficult to separate business logic parts and other internal application ones.
```java
// Do not
public class User {
    public String firstName;
    public String lastName;

    public String displayName() {
        return String.format("%s %s", firstName, lastName);
    }
}
// Do
public class User {
    public String firstName;
    public String lastName;
}
public class UserModule {
    public static String displayUserName(User user) {
        return String.format("%s %s", user.firstName, user.lastName);
    }
}
```

* I don't use other model's properties for reference  
It sounds a bit weird, but I think that most of the bugs may be caused by complecated foreign references in the most relational database.  
If it is needed to refer other model's data, I put a foreign key(such as UUID) and manipulate it by using ```module```.
```java
// Do not
public class User {
    public String name;
    public Company company;
}
// Do
public class User {
    public String name;
    public int companyID;
}
```

## 3. view

### Role
View updates UI by ```model```, and notifies user events to ```controller```.    

In android/ios, ```Activity``` and ```ViewController``` have roles of both ```View``` and ```Controller```.
It may be difficult to separate these parts.
In Android, ```ViewBinding``` will have a role of ```View```.
In ios, I separate ```ViewController``` into ```View``` and ```Controller```.

### Example

* I don't put any data class in view.
```java
// Do not
public class LoginView extends View {
    private TextView nameView;
    public User user;

    public void update() {
        this.nameView = user.name;
    }
}
// Do
public class LoginView extends View {
    private TextView nameView;

    public void update(User user) {
        this.nameView = user.name;
    }
}
```

## 4. controller  

### Role
Controller receives user events, makes ```views``` update and makes ```model``` update by using ```module```.     

I don't include any logics, because ```controller``` basically do too much roles.   
I don't include any screen transitions also. 

### Example

* I don't manipulate any views and don't manipulate any models.
```java
// Do not
public class HomeActivity implements View.OnClickListener {
    private TextView textView;
    private User user;
    public void onCreate(Bundle bundle) {
        this.user = new User("John", "2020/1/1");
        ... calculate user age
        ...
        final String displayName = new StringBuilder()
            .append(this.user.firstName)
            .append(", ")
            .append(this.user.lastName)
            .append(" age:")
            .append(this.user.age);
        this.textView = new TextView(getApplicationContext());
        this.textView.setText(displayName);
        this.textView.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        final LoginProcess process = new LoginProcess();
        process.login(this.user, new Observer() {
            @Override
            public void completed() {
                textView.setText("Completed");
            }
        });
    }
}
// Do
public class HomeActivity implements View.OnClickListener {
    private ViewBinding binding;
    private final UserModule userModule = new UserModule();

    public void onCreate(Bundle bundle) {
        this.userModule.set(new User("John", "2020/1/1"));
        this.binding.updateUser(this.userModule.displayUser());
        this.binding.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        this.userModule.login(this.binding.loginObserver);
    }
}
```


* I don't put any transition codes.     
```java
// Do not
public class HomeActivity implements View.OnClickListener {
    private User user;
    @Override
    public void onClick(View view) {
        goNextScreen(this.user);
    }
    private void goNextScreen(User user) {
        startActivity(...)
        ...
    }
}
// Do
public class HomeActivity implements View.OnClickListener {
    private User user;
    private AppCoordinator coordinator;
    @Override
    public void onClick(View view) {
        this.coodinator.nextScreen();
    }
}
```

## 5. coordinator

### Role
Coordinator switches screens, and manages controller's life cycle.

### Example

* I don't make many coordinators.
```java
// Do not
public abstract class AppCoordinator {
    void start(Activity activity, Bundle bundle);
}
public class SignupCoordinator extends AppCoordinator {
    public void start(Activity activity, Bundle bundle) {
        final Intent intent = new Intent(...).putBundle(bundle);
        activity.startActivity(intent);
    }
}
public class LoginCoordinator extends AppCoordinator {
    public void start(Activity activity, Bundle bundle) {
        final Intent intent = new Intent(...).putBundle(bundle);
        activity.startActivity(intent);
    }
}
...
// Do
public class Application {
    private AppCoordinator coordinator;

    public void dispose(Activity activity) {
        coordinator.dispose(activity);
    }
}
public class SignupActivity {
    @Override
    void finish() {
        getApplication().dispose(this);
    }
}
...
public class AppCoordinator {
    private WeakReference<Activity> reference;
    public AppCoordinator(Activity activity) {
        reference = new WeakReference<>(activity);
    }
    public void startLoginActivity() {
        final Activity activity = reference.get();
        if (activity == null) {
            return;
        }
        this.dispose();
        final Intent intent = new Intent(...).putBundle(bundle);
        activity.startActivity(intent);
    }

    public void dispose() {
        ...
    }
}
```

## 6. module

### Role
Module manipulates ```model``` and has bussiness logic.      

I don't use static fields in ```module```. ```module```'s life cycle depends on ```controller```.

### Example
```java
public class UserModule {

    private PublishSubject<Boolean> loading = PublishSubject.create();
    private APIModule apiModule = new APIModule();

    public Single<Boolean> login(User user) {
        this.loading.onNext(true);
        return this.apiModule.login(user)
            .flatMap(map -> {
                this.loading.onNext(false);
                return Single.just(true);
            });
    }
}
public class APIModule {
    public Single<User> login(User user) {
        return Single.create((SingleOnSubscribe<User>) emitter -> {
            ... apiClient.requestLogin(user);
            emitter.onSuccess(..);
        });
    }
}
```
